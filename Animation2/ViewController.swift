//
//  ViewController.swift
//  Animation2
//
//  Created by EuvicDev on 02/08/2017.
//  Copyright © 2017 EuvicDev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField1: ShakingTextField!

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {

        super.viewDidLoad()
        textField1.delegate = self

        imageView.isUserInteractionEnabled = true

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.addPulse))
        tapGestureRecognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view, typically from a nib.
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if((textField1.text?.characters.count)! < 8 || textField1.text == nil) {
            textField1.text = nil
            textField1.placeholder = "Min. length is 8 characters"
            textField1.shake()
        }

    }
    func addPulse() {
        let pulse = Pulsing(numberOfPulses: 1, radius: 100, position: imageView.center)
        pulse.animationDuration = 0.6
                pulse.backgroundColor = UIColor.orange.cgColor
        self.view.layer.insertSublayer(pulse, below: imageView.layer) }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

